import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import App from './components/App';
import Login from './components/Login';
import Bedarfsrechner from './components/Bedarfsrechner';
import Navigation from './components/Navigation';
import Registrieren from './components/Register';
import Tools from './components/Tools';
import Haushaltsbuch from './components/Haushaltsbuch';

// import * as serviceWorker from './serviceWorker';

import { HashRouter, Route } from 'react-router-dom';

ReactDOM.render(
  <HashRouter>
    <div>
      <Navigation />
      <Route exact={true} path="/" component={App} />
      <Route exact={true} path="/login" component={Login} />
      <Route exact={true} path="/bedarfsrechner" component={Bedarfsrechner}/> 
      <Route exact={true} path="/register" component={Registrieren} /> 
      <Route exact={true} path="/tools" component={Tools} />
      <Route exact={true} path="/haushaltsbuch" component={Haushaltsbuch} />

    </div>
  </HashRouter>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();
