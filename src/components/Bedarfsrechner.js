import React, { Component } from 'react';
import './Bedarfsrechner.css';
import TextField from '@material-ui/core/TextField';
import 'fontsource-roboto';
import { Link } from 'react-router-dom';

class Bedarfsrechner extends Component {
    render() {
        return (


            < div className="Bedarfsrechner" >

                <h1> Berechne hier deinen monatlichen Geldbedarf!</h1>
                <p>Deine monatlichen Ausgaben in Euro (€):</p>

                <div id="Form">
                    <form>


                        <TextField type="number" id="filled-basic" label="Miete" defaultValue="0" variant="outlined" placeholder="Euro" />

                        <TextField type="number" id="filled-basic" label="Internet/Handy" defaultValue="0" variant="outlined" placeholder="Euro" />

                        <TextField type="number" id="filled-basic" label="Transport (Auto/Öffis)" defaultValue="0" variant="outlined" placeholder="Euro" />

                        <TextField type="number" id="filled-basic" label="Nahrungsmittel" defaultValue="0" variant="outlined" placeholder="Euro" />

                        <TextField type="number" id="filled-basic" label="Versicherungen" defaultValue="0" variant="outlined" placeholder="Euro" />

                        <TextField type="number" id="filled-basic" label="Mitgliedschaften" defaultValue="0" variant="outlined" placeholder="Euro" />

                        <TextField type="number" id="filled-basic" label="Abos" defaultValue="0" variant="outlined" placeholder="Euro" />

                        <TextField type="number" id="filled-basic" label="Uni/Ausbildung" defaultValue="0" variant="outlined" placeholder="Euro" />

                        <TextField type="number" id="filled-basic" label="Kleidung" defaultValue="0" variant="outlined" placeholder="Euro" />

                        <TextField type="number" id="filled-basic" label="Sparen" defaultValue="0" variant="outlined" placeholder="Euro" />

                        <TextField type="number" id="filled-basic" label="Freizeit" defaultValue="0" variant="outlined" placeholder="Euro" />

                        <TextField type="number" id="filled-basic" label="Gesundheit/Kosmetik" defaultValue="0" variant="outlined" placeholder="Euro" />

                    </form>

                    

                </div>

                <Link to="/">Home</Link>

            </div >



        )
    }
}


export default Bedarfsrechner