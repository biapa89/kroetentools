
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import './Tools.css';

class Tools extends Component{

render() {
    return(
        <div>
            <div className="Tools">

                <header className="App-header">

                
                    <h1>Die Krötentools</h1>
                    <p>Powered by Bildungskröten</p>

                </header>

                <span>
                    <Link to="/Bedarfsrechner" className="link">Bedarfsrechner</Link> 
                </span>

                <span>
                    <Link to="/Haushaltsbuch" className="link">Haushaltsbuch</Link>
                </span>
                        
                <br></br>

                <Link to="/">Home</Link>

            </div>




        </div>
        
        )
    }
}


export default Tools