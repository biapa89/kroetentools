import React, { Component } from 'react';
import { Link } from 'react-router-dom'



class Navigation extends Component {
    render() {
        return (
            <div>

                <ul>

                        <Link to="/">Home</Link> 
                        &nbsp;| &nbsp;
                        <Link to="/Login">Login</Link>

                </ul>

            </div>


        )
    }
};

export default Navigation;