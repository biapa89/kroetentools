import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import kroetenlogo from './Kroeten_Logo_klein (1).png'
import './App.css';

class App extends Component {


    render() {
        return (
            <div className="App" >
                <header className="App-header">

                    <img src={kroetenlogo}
                        className="App-logo"
                        alt="logo" />
                    <h1>Der Kröten-Rechner</h1>
                    <p>Powered by Bildungskröten</p>

                    <div className="routes">
                        <span>
                            <Link to="/Register" className="link"> Register</Link> |
            </span>
              &nbsp;
            <span>
                            <Link to="/Login" className="link">Login</Link> |
            </span>
            &nbsp;
          
                        
                        <span>
                            <Link to="/Tools" className="link">Tools</Link> 
                        </span>

                    </div>

                    <br></br>

                    <a className="App-link"
                        href="https://www.bildungskroeten.de"
                        target="_blank"
                        rel="noopener noreferrer" >
                        Zum Blog </a>

                </header>
            </div>
        );
    }
}

export default App;
